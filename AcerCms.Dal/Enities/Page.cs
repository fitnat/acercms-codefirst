namespace AcerCms.Dal.Enities
{
    public class Page
    {        
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public bool IsDeleted { get; set; }
    }
}
